Downloading mongodb community version.

https://www.mongodb.com/try/download/community

> Temporary method 

```command 
cd C:\Program Files\MongoDB\Server\5.0

# to start the server
mongod
```

1. create a folder named data and inside data create another folder named db

```cmd
<!-- folder path -->
c:/data/db
```

Some useful mongodb command

```mongo
 <!-- show database -->
show dbs
<!-- creating database -->
use databasename 

Creating colletction/tables

db.createCollection("name")

db.name.insert({"inserthere"})

db shows the current database

pretty()


```

