## About FabFi 

It is a mesh hybrid-wireless broadband transmission infrastructure.
 
Hybrid wireless mesh networks are multi-hop networks consisting of two types of nodes,mash routers and mash clients. mash routers are static and less resource constrained that mobile mesh client, and form the wireless backhaul of the netwotk 

![mashed](./images/mash.png)

In extreme case, they mounts commercial wireless routers on Fabbed RF(radio frequency) reflectors with a wire mesh surface that redirects the RF energy. 

the reflector gain depends on the material used and the size of the reflector but has been measeured as high as 15dbi with some of the current design.

![antenna gain](./images/gain.png)

With any antenna, the apparent increase in signal is not an amplification of signal, but it is the act of redistribution of available Radio Frequency (RF) signal into a preferred direction

So basically, antennas only divert, direct, or concentrate radio energy in some direction, they don’t create it.


The single wireless link in the FabFi system consists of two reflectors with attached wireless routers.

![integration services](./images/fabfidesign.png)

the two router can be linked with a wired conncetion. a single router can be linked to both wired and wireless connections at the same time. 

a single wireless connection can reach up to 10km.

FabFi routers have four connections: LAN for local users, WAN uplink to internet, wireless signal for WifiMesh, and wired connections for mesh

![fab router](./images/fabrouter.png)

a mesh network is one where any device can be connected to one or more other neighbor devices in an unstructured (ad-hoc) manner.

mesh networks are robust and simple to configure because the software determines the routing of data automatically in real-time based on sending the network topology.

Traditional mesh networs are limited in scale because they rely on single radio, wirless-only connections and omni-direcitonal antennas. 

Fabfi can use direct wireless links and wired transfers whenever possible, 

the Fabfi project can be optimized to build scaled mashed network.

![reflector_antenna](./images/reflectorantenna.png)



