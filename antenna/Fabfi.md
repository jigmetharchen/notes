# Antenna and Radio

## Real world example

1. Jalalabad, Afganestan 

- link span ---> 6KM
- data thorughput ---> 11.5 Mbits/s
- 45 remote FabFi nodes

2. Kenya, Africa

- link span ---> 2.5 KM
- data throughput ---> 30 Mbit/s
- 50 remote Fabfi nodes

## Important components of FabFi

1. FabFi router
2. RF Reflector Antenna
3. Power Source

### FabFi Router

It uses open-sources Firmware: [OpenWRT](https://openwrt.org) 

Feature of this Routers:

1. real time network monitoring
1. local web caching centralized access control
1. user management
1. user tracking (for billing)


cost: $50-$100 USD

About OpenWRT 

* its an idea of having a modular linux distribution available on your router.
* its Open source project for embedded operating system base on linux.
* used for embedded device to route network traffic.
* used to create custom embedded operating system for routers.

[List of supported hardware on openWRT's website](http://wiki.openwrt.org/toh/start)


### RF Reflactor Antenna

Principle behind parabolic reflector antenna

y= cX^2 : when a vector tarvelling perpendicular to a parabola's directrix hits the surface of the parabola it is reflacted to the parabola's focal point

more about this in [Mathworld](https://web.archive.org/web/20111119205258/http://mathworld.wolfram.com/Parabola.html)
 
The backbone of Reflector antenaa can be

* Wood
* metal 
* Acrylic
* and we can also make by molding clay
* carve form stone etc..

Reflective surface Materials:

* chicken wire
* woven stainless steel mesh
* window screen

### Power system

* All this device run on 12VDC
* It can be directly powered by car
* we can make 12V-12V UPS which can be integrated to 100-240 VAC

Question ?

Q1. what is mean by 50 or 45 remote FabFi nodes ? Does it mean that there is 50 different Fabfi router and RF antenas? if so, how is link span of 6KM or 2.5 KM calculated? is it between two RF reflector antenna or between all 45 or 50 nodes?

Q2. Although OpenWRT can be used with multiple hardwares, which hardware was use to build FabFi routers?

Q4. Is the power system integrated with the router or we build separate power system?.

Q5. what kind of antenna is used by FabFi router to receive and transmit data?

Q6. Is FabFi using same antenna for wifiMesh to other Fabfis or require different antenna?

Q7. please Provide us with more information on FabFi Routers?



