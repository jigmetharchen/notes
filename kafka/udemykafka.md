## Topics

Topic: a particular strean of data
* similar to a table in a database
* you can have as many topic as you want
* a topic is defined by its name.

Topic are spilt in partitions:

* Each partition is ordered
* Each message within a partition gets an incremental id, called offset

When you create a topic you should write how many partitions you want. and each partitions gets an incremental id called offsets.

![](../images/Topic.png)

#### Offset

* offset only have meaning for a specific partition
* order is guranteed only within a partition
* data is kept only for a limited time(one week)
* one a data is written to a partiton, it cant be changed(immutabiltiy)
* Data is assigned randomly to a partition unless a key is provided


## Brokers

* A kafka cluster is composed of multiple brokers(servers)
* Each broker is identified with its ID (integer/ Have to be number)
* Each broker contains certain topic partitons
* After connectiong to any broker(called a bootstrap broker), you will be connected to the entire cluster

## Topic Repication

* Topic should have a replication factor( usually between 2 and 3)

* This way if a broker is down, another broker can serve the data

## Leader for a partiton

* at any time only one broker can be a leader for a given partition

* only that leader can receive and serve data for a pratiton

* The other broker wiill synchronize the data

* therefore each partition has one leader and multiple ISR(in-sync replica)

 The leader and ISR is determined by **zookeeper**

 ## Producers

 * producers write data a topic
 * producers automatically know to which broker and partition to write to 
 * in case the broker failurers, producers will automatically recover.

asks = 0; producer won's wait for aknowledgment
ask =  1; producers will wait for leader
asks = all; leader plus replicas

## Producers: Message keys:

producers can choose to send a key with the message.

## consumers:

* consumers read data from a topic
* consumers know whicj broker to read from
* in case the broker failures consumers know how to recover
* data in read in order within each partitons

## Delivery sememntics for consumers

* consumers choose when to commit offsets
* There are 3 delivery semantics
    * at most onces:
    * at least onces
    * exactly onces

## Kafka broker discovery

* every kafka broker is also called a bootstrap server
* that means that you only need to connect to one broker and you will be connected to the entire cluster
* each broker know about all borkers, topics and partitions(,etadata)

## Zookeeper

* zookeeper manages brokers
* zookeepers helps on performing leader election for partitons
* zookeeper sends notififcations to kafka in case of changes
* kafka cant work without zookeeper
* zookeeper by design operates with an odd number of servers

## kafka Guarantess

* the message are appended to a topic-partition in the order they are sent
* consusmers read messages in the order stored in a topic-partition
* with a replication factor of N, producers and consumers can tolerate up to N-1 broker being down.
* this is why a replication factor of 3 is a good idea
    * allows for one broker to be taken down for maintenance
    * allows for another broker to be taken down unexpectedly
* As long as the number of partitions remains constant for a topic(no new partitions), the same key will always go to the same partition.

## Idempotent Producer

in kafka >= 0.11, you can define a "idempotent producer" which won't introduce duplucates on network error.

it is greate way ti guatantee a stable and safe pipeline.

//create safe producers

## message compression

Producer ususally send data that is text-based, for example with JSON data
in this case, it is important to apply compression to the producer

compression is enabled at the producer level and doesn't require any configuration change in the brokers or in the Consumers.

Compression is more effective the bigger the batch the batch of message being sent to kafka!



## kafka connect Introduction 

four commmon use of kafak:
source => kafka producer API   kafka connect source
kafka ==> kafka consumer,producer api  kafka stream
kafka ==> sink consumer api  kafka connect sink
kafka => app consumer api

programmer always want to import data from the same source:
   database, blockchain, IOT, MQTT, SQS, Twitter, etc..
Programmer always want yo store data in the same sinks
    s3,ElesticSearch,mongoDB Twitter, etc..




