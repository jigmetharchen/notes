
## Lenses:

Lenses is the core component bringing everything together in a unified platform allowing you to built and monitor your data pipeline. 

lenses is a data streaming platform build on top of apache kafak. allowing you to stream analyze and react to your data fast.

## Lenses SQL(LSQL): 

is a powerful SQL engine for Apache Kafka covering both batch and streaming capabilities. 
which means you can write kafka streams applications with lanses SQL or you can browse Kafka topics data at ease.

https://lenses.io/blog/2018/01/mqtt-influxdb/

socket.request.max.bytes = 104857600
ssl.endpoint.identification.algorithm = https

