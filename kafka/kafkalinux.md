1. Download and setup Java 11 JDK

```
sudo apt install openjdk-11-jdk
```
2. download & extract the kafka binaries form __the link__
or from https://kafka.apache.org/downloads

3. try kafka commands using bin/kafka-topics.sh

4. Edit PATH to include kafka (in ~/.bashrc)

```
PATH = "$PATH:/your/path/to/your/kafka/bin"
```

5. Edit zookeeper & kafka configs using a text editor

    a. zookeeper.properties
    ```
    dataDir =/your/path/to/data/zookeeper
    ```
    b. server.properties
    ```
    log.dirs=/your/path/to/data/kafka
    ```

6. start zookeeper in one terminal window

```
zookeeper-server-start.sh config/zookeeper.properties
```

7. start kafka in another terminal window

```
kafka-server-start.sh config/server.properties
```
