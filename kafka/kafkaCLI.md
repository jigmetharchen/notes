1. Start zookeepers
2. start  kafka

## Kafka CLI

Create topics
```
kafka-topic --zookeeper 172.0.0.1:2181 --topic first_topic --create --partitions 3 --replication-factor 1
```
List Topic
```
kafka-topic --zeekeeper 172.0.0.1:219 --list

kafka-topic --zeekeeper 172.0.0.1:219 --topic first_topic --describe
```
Create topic
```
kafka-topic --zeekeeper 172.0.0.1:219 --topic second_topic --create --partitons 6 --replication-factor 1
```
Delete Topic
```
kafka-topic --zookeeper 172.0.0.1:2181 --topic second_topic --delete
```