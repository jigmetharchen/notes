## Event streaming?

It is digital equivalent of the human body's central nervous system.

it is the practice of capturing data in real-time from event sources like databases, sensors,mobile devices etc..

## what can i use event streaming for?

To track and monitor cars, trucks, fleets and shipment in real-time, such as in logistics and the automotive industry

To contrinuously capture and analyze sensor data from IOT devices 

To get immediately react to coutomer interaction and orders, such as in retail, the hotel and travel intustry and mobile applicaion.


## Apache kafka is an event streaming platform. What does it mean?

1. To Publish(write) and subscribe to(read) streams of events, including continuous import/export of your data from other system

2. to store streams of events durably and reliably for as long as you want

3. to process streams of events as they occur or retrospectively.

## How does kafka work in nutshell?

Kafka is a distributed system consisting of servers and client that communicate via a high-performance TCP network protocol. It can be deployed on bare-metal hardware, VM, and containers and on-premises as well as cloud.

**Servers**: kafka is run as a cluster of one or more servers that can span multiple datacenters or clouds regions. some of these servers from the storage layer, called the brokers. other servers run kafka connects to continuoslt import and export data as event streams tointegrate kafka with your existing system.

**Client**: They allows you to write distributed applicaitons and microservices to read, write and process streams of events in parallel, at scale and in a fault-tolerant manner even in the case of network problem or machine failure.

## Main concepts and terminology

**Events/record/messages**: it records the facts that something happend in the world or in your business.

Event has a key, value and timestamp

 . Event key: "Alice"
 . Event value: "Made a payment of $200 to Bob"
 . Event timestamp: "Jun. 25, 2020 at 2:06 p.m."

**Producers** are those client aapplications that publish events to kafka, and **consumers** are those that subscribe to these events.

Events are organized and durably stored in **topics**.  Very simplified, a topic is similar to a folder in a filesystem, and the vents are the files in that folder. 

Topic in kafka are always multi-producer and multi-subscriber.

Events in a topic can be read as often as needed and is retain as per-topic configuration setting.

Topic are **Partitioned**, meaning a topic is spread over a number of "buckets" located on different kafka brokers.

![partitioned_kafka](./images/partitioned_kafka.png)


To make your data fault-tolereant and highly-available, every topic can be **replicated** even across gro-regions or datacenters.

## Kafka APIs

1. Admin API to manage and inspect topics, brockers and other kafka objects

1. Producer API to publish a stream of events to on or more kafka topic

1. consumer API to subscribe to one or more topics and to process the stream of events produced to them.

1. Kafka streams API: 

1. The kafka connect api : to build and run reusable data import/export connectors that consume or produce streams of events from and to external systems and applications so they can integrate with kafka. 