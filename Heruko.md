1. create heruko account
2. change port

```
using .env file
```
3. Procfile
> it tell heroku which file to run just like saying 

nodemon app.js 

when we do ```npm start``` it runs app.js file.

4. Pushing to heroku

Create a new Git repository
Initialize a git repository in a new or existing directory

```bash
cd my-project/
git init
heroku git:remote -a sfl-web
```
Deploy your application
Commit your code to the repository and deploy it to Heroku using Git.

```bash
git add .
git commit -am "make it better"
git push heroku master
```