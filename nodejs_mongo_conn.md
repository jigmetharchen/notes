# Nodejs mongo connection

1. install package mongoose

```bash
npm install mongoose --save
```
2. create file name connection.js

```javascript
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});

mongoose.connection.once('open',function(){
    console.log('connection has been made');
}).on('error', (err)=>{
    console.log('error is:', err)
})

```

3. Connecting Local nodejs app with mongodb altas.

```
<!-- connection string to mongodb cloud -->

mongodb+srv://jigme:<password>@cluster0.9vbn9.mongodb.net/DatabaseName?retryWrites=true&w=majority

```

4. Connection heroku with mogodb

```javascript

1. set up heroku enviroument

Go ro project --> setting --> config vars

insert MONGODB_URI = mongodb+srv://jigme:<password>@cluster0.9vbn9.mongodb.net/DatabaseName?retryWrites=true&w=majority

const MONGODB_URI = mongodb+srv://jigme:<password>@cluster0.9vbn9.mongodb.net/DatabaseName?retryWrites=true&w=majority
const url = process.env.MONGODB_URI || MONGODB_URI

mongoose.connect(url, useNewUrlParser: true);

```